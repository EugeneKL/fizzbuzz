function fizzbuzz(maxValue) {
    const string = [];
    for (let i = 1; i <= maxValue; i++) {
        if (i % 2 === 0 && i % 3 ===0) {
            string.push('FizzBuzz, ');
        } else if (i % 2 === 0) {
            string.push('Fizz, ');
        } else if (i % 3 === 0) {
            string.push('Buzz, '); 
        } else {
            string.push(i + ', ');
        }   
    }
    return string;
}
console.log(fizzbuzz(6));